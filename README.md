# Kashable QA Engineer Assessment v1.0.0

Welcome to Kashable's QA Engineer interview test. For this test, you will be implementing the 
tests in the **tests** directory. 

The tests are designed to take 45 to 90 minutes to implement. You're encourages to look at the documentation for CodeceptJS  with Playwright:

https://codecept.io/playwright/ and https://codecept.io/helpers/Playwright/

## REQUIREMENTS
NODE JS 14.X AND NPM 6.X (may work on older versions)
Firefox Browser must be installed

For details on installing the above, please visit: 

https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

## SETUP
In the main directory, run:

`npm install`

## RUN THE TESTS
`npm run codeceptjs`