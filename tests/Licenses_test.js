Feature("Licenses");

/**
 * 1) Navigate to https://kashable.com/compliance.html
 * 2) Make sure all the image are working
 */
Scenario("state flag images are not broken", async ({ I }) => {
  I.amOnPage("/compliance.html");
  I.waitNumberOfVisibleElements('img[src$=".jpg"]', 41);
  I.seeNumberOfVisibleElements('img[src$=".jpg"]', 41);
  I.saveScreenshot("pictures.png", true);
});
