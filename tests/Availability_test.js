/**
 * 1) Navigate to https://kashable.com
 * 2) Press "Check Now" button under section titled "Is Kashable Available at your Job?"
 * 3) Perform some action on the typeahead
 * 4) Do not press "Check Now". It is protected by reCaptcha
 */
Feature("Employer Availability");

Scenario('"USPS" search returns US Postal Service', ({ I }) => {
  I.amOnPage("/");
  I.click("Check Now");
  I.fillField("employer", "USPS");
  I.see("United States Postal Service", { css: ".typeahead__list" });
});

Scenario('"xyz" shows "No results found"', ({ I }) => {
  I.amOnPage("/");
  I.click("Check Now");
  I.fillField("employer", "xyz");
  I.see("No results found", { css: ".typeahead__list" });
});
