Feature("Confirm contact info is correct (footer of main page)");

Scenario("Support number text in footer is 646-663-4353", ({ I }) => {
  I.amOnPage("/");
  I.see("(646) 663‑4353", { css: ".footer__menu-list .phone" });
});

Scenario("Support email in footer is support@kashable.com", ({ I }) => {
  I.amOnPage("/");
  I.see("support@kashable.com", ".footer__menu-list .blue");
  I.seeAttributesOnElements(".footer__menu-list .blue", { href: "mailto:support@kashable.com" });
});
