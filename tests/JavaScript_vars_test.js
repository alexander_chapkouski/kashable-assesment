Feature("Test to make sure JavaScript variables are in scope");

/**
 * 1) Navigate to https://kashable.com
 * 2) Execute the following JavaScript: "jQuery().jquery"
 * 3) Result should be '3.5.1'
 */
Scenario("Check jQuery version", async ({ I }) => {
  I.amOnPage("/");
  const string = await I.executeScript(() => jQuery().jquery);
  const result = string === "3.5.1" ? "Pass" : "Fail";
  console.log(result);
});

/**
 * 1) Navigate to https://kashable.com
 * 2) Execute the following JavaScript: "ga"
 * 3) Result should not be undefined. This variable is loaded by a script that runs asynchronously
 */
Scenario("Make sure Google Analytics has loaded successfully", async ({ I }) => {
  I.amOnPage("/");
  const string = await I.executeScript(() => typeof ga);
  const result = string === "function" ? "Pass" : "Fail";
  console.log(result);
});
